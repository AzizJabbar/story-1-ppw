from django import forms
from .models import Matkul, Delete
from django.forms import ModelForm
class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'

class DeleteForm(ModelForm):
    class Meta:
        model = Delete
        fields = '__all__'

