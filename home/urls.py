from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('write', views.write, name='write'),
    path('about', views.about, name='about'),
    path('story1', views.story1, name='story1'),
    path('edit', views.edit, name='edit'),
    path('matkul', views.matkul, name='matkul'),
    path('success', views.success, name='success'),
    path('delete', views.delete, name='delete'),
    #path('detail', views.detail, name='detail'),
    url(r'^detail/$', views.detail ),
]