from django.db import models

# Create your models here.
class Matkul(models.Model):
    matkul_name = models.CharField(max_length=50)
    lecturer_name = models.CharField(max_length=50)
    sks = models.IntegerField(max_length=2)
    matkul_desc =  models.TextField(max_length=100)
    acad_year = models.CharField(max_length=50)
    room = models.CharField(max_length=10)

class Delete(models.Model):
    del_name = models.CharField(max_length=50)